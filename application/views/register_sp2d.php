 <!DOCTYPE html>
<!-- <meta http-equiv="refresh" content="5; url='http://localhost:8080/registersp2d/rinci'"> -->
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="30">
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <title>REGISTER SP2D</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">


    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <style type="text/css">

table, th, td {
  border: 3px solid #f59e42;
}

    @font-face {
         font-family: "DK";
         src: url('assets/font/DK.otf');
         }
 
   .DK {
         font-family: "DK";
         }    
    @font-face {
         font-family: "Gobold";
         src: url('assets/font/Gobold.otf');
         }
 
   .Gobold {
         font-family: "Gobold";
         }
    @font-face {
         font-family: "AGENCYR";
         src: url('assets/font/AGENCYR.TTF');
         }
 
   .AGENCYR {
         font-family: "AGENCYR";
         } 

    #garis {
          border: 5px solid #f59e42;
          padding: 10px;
          border-radius: 25px;
        }

body{
          
  background-image: url("assets/images/lima.jpeg"); /* The image used */
  background-color: #cccccc; /* Used if the image is unavailable */
  height: 500px; /* You must set a specified height */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
}

</style>
</head>
<body>
    <header id="header" class="header">
        <div class="header-menu">
            <div>
                <center><div class="Gobold" style="font-size: 80px;">REGISTER SP2D</div></center>
            </div>  
        </div>
    </header>


    <div class="content mt-3">
        <div class="col-sm-6 col-lg-3  ">
            <div class="" style="opacity: 80%">
                <div class="card-body pb-0" id="garis" style="background-color: #9999FF">
                    <h1 class="DK" style="color:" >SKPD</h1>
                    <?php
                    $query = $this->db->query("SELECT COUNT( * ) as total FROM DAFTUNIT where KDLEVEL='3' and NMUNIT not in ('dprd')");$row = $query->row();
                    ?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total;?></span></center>
                    </h4>
                    <center>
                        <h3 style="font-size: 15px; color: black; font-weight: bold; margin-bottom: 5%">TOTAL SATUAN KERJA PERANGKAT DAERAH PEMERINTAH KOTA BATAM</h3>
                    </center>
                </div>
            </div>
        </div>
         
         <div class="col-sm-6 col-lg-3">
            <div class="" style="opacity: 80%">
                <div class="card-body pb-0" id="garis" style="background-color:#9999FF">
                    <h1 class="DK" style="color:" >SPM</h1>
                    <?php
                    $query = $this->db->query("SELECT COUNT( * ) as total FROM ANTARBYR WHERE PENOLAKAN ='1'"); $row = $query->row();?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total;?></span></center>
                    </h4>
                    <center>
                        <h3 style="font-size: 15px; color: black; font-weight: bold; margin-bottom: 5%">TOTAL SPM YANG TERBIT YANG ADA DI PEMERINTAH DAERAH KOTA BATAM</h3>
                    </center>
                </div>
            </div>
        </div>
           

        <div class="col-sm-6 col-lg-3">
            <div class="" style="opacity: 80%">
                <div class="card-body pb-0" id="garis" style="background-color:#9999FF">
                    <h1 class="DK" style="color:" >SP2D TERBIT</h1>
                    <?php
                    $query = $this->db->query("SELECT COUNT(nosp2d) as total FROM SP2D WHERE PENOLAKAN ='1'"); $row = $query->row();?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total;?></span></center>
                    </h4>
                    <center>
                        <h3 style="font-size: 15px; color: black; font-weight: bold; margin-bottom: 5%">TOTAL SP2D YANG TERBIT YANG ADA DI PEMERINTAH DAERAH KOTA BATAM</h3>
                    </center>
                </div>
            </div>
        </div>
           

        <div class="col-sm-6 col-lg-3">
            <div class="" style="opacity: 80%">
                <div class="card-body pb-0" id="garis" style="background-color:#9999FF">
                    <h1 class="DK" style="color:" >SP2D CAIR</h1>
                    <?php $query = $this->db->query("SELECT COUNT( * ) as total FROM BKUK"); $row = $query->row();?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px;"><?php echo $row->total;?></span></center>
                    </h4>
                    <center>
                        <h3 style="font-size: 15px; color: black; font-weight: bold; margin-bottom: 5%">TOTAL SP2D YANG CAIR YANG ADA DI PEMERINTAH DAERAH KOTA BATAM</h3>
                    </center>
                </div>
            </div>
        </div>
    </div>


    <!-- SP2D HARI INI SEBELAH KANAN -->
    <div class=" content mt-5">
        <div class="col-md-2 float-right" style="margin-right: 4%">
            <center><h1 class="DK" style="color:">SP2D HARI INI</h1></center>
                <div id="garis" style="opacity: 80%; background-color:#9999FF">
                    <div class="card-body pb-0">
                        <center><h1 class="DK" >TERBIT</h1></center>
                        <?php
                        $query = $this->db->query("SELECT COUNT( * ) as total FROM SP2D where TGLSP2D=convert(date,getdate())");$row = $query->row();?>
                        <h4 class="mb-0">
                            <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total;?></span></center>
                        </h4>
                    </div>
                    <div class="card-body pb-0">
                        <center><h1 class="DK" >CAIR</h1></center>
                         <?php
                        $query = $this->db->query("SELECT COUNT( * ) as total FROM BKUK where TGLKAS=convert(date,getdate())");$row = $query->row();?>
                        <h4 class="mb-0">
                            <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total;?></span></center>
                        </h4>
                    </div>
                </div>
        </div>

        <div class="col-xl-9 col-lg-3 float-left">
            <div class="container">
            <center><h1 class="DK" style="color: ">REALISASI BELANJA SP2D</h1></center><br>
             <center><h1 class="DK" style="margin-top: -25px">PEMERINTAH KOTA BATAM TAHUN ANGGARAN 2019</h1></center><br>
                <center><div class="table-responsive">
                    <table>
                        <thead>
                            <tr>
                                <th class="AGENCYR" style="font-size: 50px; font-weight: bold; "><center>URAIAN</center></th>
                                <th class="AGENCYR" style="font-size: 50px; font-weight: bold; "><center>ANGGARAN</center></th>
                                <th class="AGENCYR" style="font-size: 50px; font-weight: bold; "><center>REALISASI</center></th>
                                <th class="AGENCYR" style="font-size: 50px; font-weight: bold; "><center>%</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; ">BELANJA</td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>2.800.000.000,00</center></td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>2.800.000.000,00</center></td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>100</center></td>
                            </tr>
                            <tr>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; ">BELANJA TIDAK LANGSUNG</td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>1.200.000.000,00</center></td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>1.200.000.000,00</center></td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>100</center></td>
                            </tr>
                            <tr>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; ">BELANJA LANGSUNG</td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>1.600.000.000,00</center></td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>1.600.000.000,00</center></td>
                                <td class="AGENCYR" style="font-size: 40px; font-weight: bold; "><center>100</center></td>
                            </tr>
                        </tbody>
                    </table></center>
                </div>
            </div>
        </div>
     </div>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/widgets.js"></script>
</body>
</html>
