<!DOCTYPE html>
<!-- <meta http-equiv="refresh" content="10; url=''"> -->
<!-- <meta http-equiv="refresh" content="5; url='http://localhost:8080/registersp2d/register'"> -->
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="30">
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <title>REGISTER SP2D</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">


    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <style type="text/css">
    @font-face {
         font-family: "DK";
         src: url('assets/font/DK.otf');
         }
 
   .DK {
         font-family: "DK";
         }    
    @font-face {
         font-family: "Gobold";
         src: url('assets/font/Gobold.otf');
         }
 
   .Gobold {
         font-family: "Gobold";
         }
    @font-face {
         font-family: "AGENCYR";
         src: url('assets/font/AGENCYR.TTF');
         }
 
   .AGENCYR {
         font-family: "AGENCYR";
         } 

    #garis {
          border: 1px solid black;
          padding: 10px;
          border-radius: 0px;
        }
    .konten{
            height: 300px;
        }


body{
          
  background-image: url("assets/images/lima.jpeg"); /* The image used */
  background-color: #cccccc; /* Used if the image is unavailable */
  height: 500px; /* You must set a specified height */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */


</style>


</head>
<body>
    <header id="header" class="header">
        <div class="header-menu">
            <div>
                <center><div class="Gobold" style="font-size: 80px;">RINCI</div></center>
            </div>  
        </div>
    </header>

<div class="row konten">
    <div class="col-md-4" style="background-color: #9999FF;" id="garis">
        <center><h1 class="DK" style="margin-top:30px" >SP2D UP</h1></center>
                    <?php
                        $query= $this->db->query("select COUNT(A.NOSP2D) total from SP2D a
                                left join SP2DDETB b on a.UNITKEY=b.UNITKEY and a.NOSP2D=b.NOSP2D
                                left join MATANGB c on b.MTGKEY=c.MTGKEY
                                left join DAFTUNIT d on a.UNITKEY=d.UNITKEY
                                left join BKUK e on a.UNITKEY=e.UNITKEY and a.NOSP2D=e.NOSP2D
                                where a.PENOLAKAN='1' and MONTH(a.tglsp2d)<='12' and a.KDSTATUS='21' and a.IDXKODE='6'");
                                $row=$query->row();

                        $query2=$this->db->query("select sum(b.NILAI) total2 from SP2D a
                                left join SP2DDETB b on a.UNITKEY=b.UNITKEY and a.NOSP2D=b.NOSP2D
                                left join MATANGB c on b.MTGKEY=c.MTGKEY
                                left join DAFTUNIT d on a.UNITKEY=d.UNITKEY
                                left join BKUK e on a.UNITKEY=e.UNITKEY and a.NOSP2D=e.NOSP2D
                                where a.PENOLAKAN='1' and MONTH(a.tglsp2d)<='12' and a.KDSTATUS='21' and a.IDXKODE='6'");
                                $row2=$query2->row();
                            
                    ?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total;?></span></center>
                    </h4>


                    <center>
                        <h3 style="font-family: 'agencyr', 'Times', Sans-serif;font-size: 50px; color: black; font-weight: bold; margin-bottom: 5%"><?php 
                        function convert_to_rupiah($angka)
                        {
                            // return 'Rp. '.strrev(implode(str_split(strrev(strval($angka)),2,'.','')));
                            return $angka_format = number_format($angka,2);
                        }
                    echo "Rp.".convert_to_rupiah ($row2->total2);?></h3>   
                    </center>

    </div>
    <div class="col-md-4" style="background-color: #f59e42" id="garis">
        <center><h1 class="DK" style="margin-top:30px" >SP2D GU</h1></center>
                    <?php
                    $query= $this->db->query("select COUNT(nosp2d) TOTAL_SP2D from SP2D where NOSP2D like '%gu%'");
                                $row=$query->row();

                    $query2=$this->db->query("select sum(b.NILAI) SP2D_GU from SP2D a
                    left join  SP2DDETR b on a.UNITKEY=b.UNITKEY and a.nosp2d=b.nosp2d where b.NOSP2D like '%gu%' ");
                                $row2=$query2->row();
                    ?>

                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->TOTAL_SP2D;?></span></center>
                    </h4>


                    <center>
                        <h3 style="font-family: 'agencyr', 'Times', Sans-serif;font-size: 50px; color: black; font-weight: bold; margin-bottom: 5%"><?php 
                             echo "Rp.".convert_to_rupiah ($row2->SP2D_GU);?></h3>    
                    </center> 

    </div>
    <div class="col-md-4" style="background-color: #9999FF;" id="garis">
        <center><h1 class="DK" style="margin-top:30px" >SP2D TU</h1></center>
                    <?php
                    $query= $this->db->query("select COUNT(nosp2d) TOTAL_SP2D from SP2D where NOSP2D like '%tu%'");
                                $row=$query->row();
                  

                      $query2= $this->db->query("select sum(b.NILAI) SP2D_TU from SP2D a
                                left join SP2DDETR b on a.UNITKEY=b.UNITKEY and a.NOSP2D=b.NOSP2D
                                left join MATANGR c on b.MTGKEY=c.MTGKEY
                                left join DAFTUNIT d on a.UNITKEY=d.UNITKEY
                                left join BKUK e on a.UNITKEY=e.UNITKEY and a.NOSP2D=e.NOSP2D
                                where a.PENOLAKAN='1' and MONTH(a.tglsp2d)<='12' and a.KDSTATUS='23' and a.IDXKODE='2'");
                                $row2=$query2->row();
                    
                    ?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->TOTAL_SP2D;?></span></center>
                    </h4>
                    <center>
                        <h3 style="font-family: 'agencyr', 'Times', Sans-serif;font-size: 50px; color: black; font-weight: bold; margin-bottom: 5%">
                            <?php echo "Rp.".convert_to_rupiah ($row2->SP2D_TU);?></h3>   
                    </center>

    </div>
</div>
<div class="row konten">
    <div class="col-md-4" style="background-color: #f59e42" id="garis">
        <center><h1 class="DK" style="margin-top:30px" >SP2D LS-BJ</h1></center>
                    <?php
                    $query= $this->db->query("select COUNT(NOSP2D) total3 from SP2D where IDXKODE='2' and KDSTATUS='24'");
                                $row=$query->row();

                    $query2= $this->db->query("select sum(b.NILAI) SP2D_LSBJ from SP2D a
                                left join SP2DDETR b on a.UNITKEY=b.UNITKEY and a.NOSP2D=b.NOSP2D
                                left join MATANGR c on b.MTGKEY=c.MTGKEY
                                left join DAFTUNIT d on a.UNITKEY=d.UNITKEY
                                left join BKUK e on a.UNITKEY=e.UNITKEY and a.NOSP2D=e.NOSP2D
                                where a.PENOLAKAN='1' and MONTH(a.tglsp2d)<='12' and a.KDSTATUS='24' and a.IDXKODE='2'");
                                $row2=$query2->row();
                    ?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total3;?></span></center>
                    </h4>
                    <center>
                        <h3 style="font-family: 'agencyr', 'Times', Sans-serif;font-size: 50px; color: black; font-weight: bold; margin-bottom: 5%">
                            <?php echo "Rp.".convert_to_rupiah ($row2->SP2D_LSBJ);?></h3>
                    </center>

    </div>
    <div class="col-md-4" style="background-color: #9999FF;" id="garis">
        <center><h1 class="DK" style="margin-top:30px" >SP2D LS-GT</h1></center>
                    <?php
                    $query= $this->db->query("select COUNT(NOSP2D) total4 from SP2D
                                where PENOLAKAN='1' and MONTH(tglsp2d)<='12' and KDSTATUS='24' and IDXKODE='4'");
                                $row=$query->row();

                     $query2= $this->db->query("select sum(b.NILAI) SP2D_GT from SP2D a
                                left join SP2DDETRTL b on a.UNITKEY=b.UNITKEY and a.NOSP2D=b.NOSP2D
                                left join MATANGR c on b.MTGKEY=c.MTGKEY
                                left join DAFTUNIT d on a.UNITKEY=d.UNITKEY
                                left join BKUK e on a.UNITKEY=e.UNITKEY and a.NOSP2D=e.NOSP2D
                                where a.PENOLAKAN='1' and MONTH(a.tglsp2d)<='12' and a.KDSTATUS='24' and a.IDXKODE='4'");
                                $row2=$query2->row();
                                
                    ?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total4;?></span></center>
                    </h4>
                    <center> 
                        <h3 style="font-family: 'agencyr', 'Times', Sans-serif;font-size: 50px; color: black; font-weight: bold; margin-bottom: 5%">
                            <?php echo "Rp.".convert_to_rupiah ($row2->SP2D_GT);?></h3>
                    </center>

    </div>
    <div class="col-md-4" style="background-color: #f59e42" id="garis">
        <center><h1 class="DK" style="margin-top:30px" >SP2D HIBAH BANSOS</h1></center>
                    <?php
                    $query= $this->db->query("select COUNT(A.NOSP2D) total5 from SP2D a
                                left join SP2DDETRTL b on a.UNITKEY=b.UNITKEY and a.NOSP2D=b.NOSP2D
                                left join MATANGR c on b.MTGKEY=c.MTGKEY
                                left join DAFTUNIT d on a.UNITKEY=d.UNITKEY
                                left join BKUK e on a.UNITKEY=e.UNITKEY and a.NOSP2D=e.NOSP2D
                                where a.PENOLAKAN='1' and MONTH(a.tglsp2d)<='12' and a.KDSTATUS='24' and a.IDXKODE='3'");
                                $row=$query->row();

                    $query2= $this->db->query("select sum(b.NILAI) SP2D_HIBAHBANSOS from SP2D a
                                left join SP2DDETRTL b on a.UNITKEY=b.UNITKEY and a.NOSP2D=b.NOSP2D
                                left join MATANGR c on b.MTGKEY=c.MTGKEY
                                left join DAFTUNIT d on a.UNITKEY=d.UNITKEY
                                left join BKUK e on a.UNITKEY=e.UNITKEY and a.NOSP2D=e.NOSP2D
                                where a.PENOLAKAN='1' and MONTH(a.tglsp2d)<='12' and a.KDSTATUS='24' and a.IDXKODE='3'");
                                $row2=$query2->row();
                    ?>
                    <h4 class="mb-0">
                        <center><span class="count AGENCYR" style="font-size: 100px"><?php echo $row->total5;?></span></center>
                    </h4>
                    <center>
                        <h3 style="font-family: 'agencyr', 'Times', Sans-serif;font-size: 50px; color: black; font-weight: bold; margin-bottom: 5%">
                            <?php echo "Rp.".convert_to_rupiah ($row2->SP2D_HIBAHBANSOS);?></h3>
                    </center>

    </div>
</div><br>
   
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/widgets.js"></script>
</body>
</html>
